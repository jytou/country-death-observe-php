<?php
function connect($host, $db)
{
	$ini_array = parse_ini_file("/share/web/obsfr/connect_$host.ini");
	if (array_key_exists("socket", $ini_array))
		$connect = new mysqli($ini_array["host"], $ini_array["user"], $ini_array["pass"], $db == NULL ? $ini_array["database"] : $db, 0, $ini_array["socket"]) or die(mysqli_connect_error());
	else
		$connect = new mysqli($ini_array["host"], $ini_array["user"], $ini_array["pass"], $db == NULL ? $ini_array["database"] : $db, $ini_array["port"]) or die(mysqli_connect_error());
	return $connect;
}
?>
