<html>
<script src="jquery-1.6.2.min.js"></script>
<script src="jquery.imagemapster.js"></script>
<script src="browser-polyfill.min.js"></script>
<body>
<img src="carte.png.php" usemap="#deps" width="508" height="500"/>
<map name="deps">
<?php
require_once 'connect.php';
$conn = connect("aidesk2", "obsfr");
$s = $conn->prepare("select id_departement, poly, departement from departement where poly is not null and poly<>''") or die($conn->error);
$s->execute();
$s->bind_result($idDpt, $poly, $dpt);
while ($s->fetch())
{
	echo "<area shape=\"poly\" coords=\"$poly\" state=\"$idDpt\" title=\"$dpt ($idDpt)\" class=\"{fill:true,fillColor:'cd3333',fillOpacity:1,stroke:true,strokeColor:'003333',strokeOpacity:0.8,strokeWidth:1}\" alt=\"$dpt\">\n";
}
$s->close();
?>
</map>
<script type="text/javascript">
var basic_opts = {
	    mapKey: 'state'
	};

	var initial_opts = $.extend({},basic_opts,
	    {
	        staticState: true,
	        fill: true,
	        fillColor: '660000',
	        fillOpacity: 1,
	        stroke: false,
	        strokeWidth: 2,
	        strokeColor: '880000'
	    });

	$('img').mapster(initial_opts)
	    .mapster('set',true,'2A000', {
	        fill: true,
	        fillColor: '0000ff',
	        stroke: true
	    })
	    .mapster('snapshot')
	    .mapster('rebind',basic_opts);

</script>
</body>
</html>