<!DOCTYPE html>
<html>
<head>
  <title>jVectorMap demo</title>
  <link rel="stylesheet" href="jquery-jvectormap-2.0.5.css" type="text/css" media="screen"/>
  <!--script src="jquery-1.6.2.min.js"></script-->
  <script
			  src="https://code.jquery.com/jquery-1.12.4.min.js"
			  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
			  crossorigin="anonymous"></script>
  <script src="jquery-jvectormap-2.0.5.min.js"></script>
  <script src="jquery-jvectormap-fr-mill.js"></script>
</head>
<body>
  <div id="france-map" style="width: 800px; height: 984px"></div>
  <script>
  var dptData = {
<?php
require_once 'connect.php';
$conn = connect("aidesk2", "obsfr");

$s = $conn->prepare("select id_departement, departement from departement where poly is not null and poly<>''") or die($conn->error);
$s->execute();
$s->bind_result($idDpt, $dpt);
$dptNames = array();
while ($s->fetch())
{
	$dptNames[$idDpt] = $dpt;
}
$s->close();

$first = true;
foreach ($dptNames as $idDpt => $dptName)
{
	//$s = $conn->prepare("select yyyy, nb from dstat$idDpt where yyyy>='2017' and yyyy<='2020' and concat(mm, dd)>='0315' and concat(mm, dd)<='0415' and age is null") or die($conn->error);
	$s = $conn->prepare("select yyyy, nbpond from dstat$idDpt where yyyy>='2000' and yyyy<='2005' and concat(mm, dd)>='0115' and concat(mm, dd)<='0315' and age is null") or die($conn->error);
	$s->execute();
	$s->bind_result($yyyy, $nb);
	$avgOthers = 0;
	$nbOthers = 0;
	$avg2003 = 0;
	$nb2003 = 0;
	while ($s->fetch())
	{
		if ($yyyy == "2000")
		{
			$nb2003++;
			$avg2003 += $nb;
		}
		else
		{
			$nbOthers++;
			$avgOthers += $nb;
		}
	}
	$s->close();
	if (($nb2003 > 0) && ($nbOthers > 0))
	{
		$avgOthers /= $nbOthers;
		$avg2003 /= $nb2003;
		if ($first)
			$first = false;
		else
			echo ", ";
		echo "\"FR-".substr($idDpt, 0, 2)."\": ".(100.0 * $avg2003 / $avgOthers);
	}
}

?>
};
    $(function(){
      $('#france-map').vectorMap({
          map: 'fr_mill',
          series: {
              regions: [{
                  values: dptData,
                  scale:["#ffffff", "#0000ff"],
                  normalizeFunction: "polynomial"
              }]
          }
      });
    });
  </script>
</body>
</html>