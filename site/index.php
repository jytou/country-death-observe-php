<html>
<head>
<script src="Chart.min.js"></script>
 <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<?php
function createOption($val, $show, $selected)
{
	echo '<option value="'.$val.'"';
	if ($val == $selected)
		echo " selected";
		echo '>'.$show.'</option>';
}

$graphType = '0';
if (isset($_POST["graphType"]))
	$graphType = intval($_POST["graphType"]);
$yearIn = 0;
if (isset($_POST["yearIn"]))
	$yearIn = intval($_POST["yearIn"]);
$yearOut = 0;
if (isset($_POST["yearOut"]))
	$yearOut = intval($_POST["yearOut"]);
$monthIn = 0;
if (isset($_POST["monthIn"]))
	$monthIn = intval($_POST["monthIn"]);
$monthOut = 0;
if (isset($_POST["monthOut"]))
	$monthOut = intval($_POST["monthOut"]);
$ageRange = -1;
if (isset($_POST["ageRange"]))
	$ageRange = intval($_POST["ageRange"]);
$dpt = "00000";
if (isset($_POST["dpt"]))
	$dpt = $_POST["dpt"];
?>
<form id="myform" action="index.php" method="post">
Type <select name="graphType" onchange="javascript:getElementById('myform').submit();">
<?php
createOption("0", "Absolu", $graphType);
createOption("1", "Pond&eacute;r&eacute;", $graphType);
createOption("2", "&Eacute;tal&eacute;", $graphType);
?>
</select>
From <select name="yearIn" onchange="javascript:getElementById('myform').submit();">
<?php
createOption(0, "All", $yearIn);
for ($i = 2000; $i <= 2020; $i++)
	createOption($i, $i, $yearIn);
?>
</select>
To <select name="yearOut" onchange="javascript:getElementById('myform').submit();">
<?php
createOption(0, "All", $yearOut);
for ($i = 2000; $i <= 2020; $i++)
	createOption($i, $i, $yearOut);
?>
</select>
From <select name="monthIn" onchange="javascript:getElementById('myform').submit();">
<?php
createOption(0, "All", $monthIn);
for ($i = 1; $i <= 12; $i++)
	createOption($i, $i, $monthIn);
?>
</select>
To <select name="monthOut" onchange="javascript:getElementById('myform').submit();">
<?php
createOption(0, "All", $monthOut);
for ($i = 1; $i <= 12; $i++)
	createOption($i, $i, $monthOut);
?>
</select>
Age <select name="ageRange" onchange="javascript:getElementById('myform').submit();">
<?php
createOption(-1, "All", $ageRange);
for ($i = 0; $i <= 9; $i++)
	createOption($i, "".($i*10)."-".($i == 9 ? 120 : $i*10+10), $ageRange);
?>
</select>
D&eacute;partement <select name="dpt" onchange="javascript:getElementById('myform').submit();">
<?php
createOption("00000", "All", $dpt);
require_once 'connect.php';
$conn = connect("aidesk2", "obsfr");
$s = $conn->prepare("select id_departement, departement from departement") or die($conn->error);
$s->execute();
$s->bind_result($idDpt, $dptName);
while ($s->fetch())
	createOption($idDpt, $idDpt." - ".$dptName, $dpt);
$s->close();
?>
</select>

<input type="submit" name="Go!">
</form>
<?php
$startGen = time();

function createChart($conn, $chartType, $id, $title, $width, $height, $sql, $types, $params)
{
	echo '<div width="'.$width.'px" height="'.$height.'px" style="width:'.$width.'px;height='.$height.'px"><canvas id="'.$id.'" width="'.$width.'" height="'.$height.'"></canvas></div>';
	$s = $conn->prepare($sql) or die($conn->error);
	if (count($params) == 1)
		$s->bind_param($types, $params[0]);
	else if (count($params) == 2)
		$s->bind_param($types, $params[0], $params[1]);
	else if (count($params) == 3)
		$s->bind_param($types, $params[0], $params[1], $params[2]);
	else if (count($params) != 0)
		die("Number of parameters not supported for chart ".$title);
	$s->execute();
	$s->bind_result($group, $year, $month, $day, $value, $color);
	$labels = "";
	$curGroup = NULL;
	$values = "";
	$collectingLabels = true;
	while ($s->fetch())
	{
		if ($collectingLabels)
		{
			if ($labels != "")
				$labels .= ",";
			$labels .= "'$month/$day'";
		}
		if ($group != $curGroup)
		{
			if ($curGroup != NULL)
			{
				$collectingLabels = false;
				$values .= "]},";
			}
			$values .= "{label:'$group',order:'$group',fill:false,borderColor:'#$color',data:[";
			$curGroup = $group;
		}
		else
			$values .= ",";
		$values .= $value == "" ? "0" : number_format($value, 2, '.', '');
	}
	if (count($values) > 0)
		$values .= "]}";
	$s->close();
	echo "<script>new Chart(document.getElementById(\"".$id."\").getContext(\"2d\"), {\ntype: '".$chartType."',\ndata: {\nlabels: [".$labels."],\ndatasets: [$values]},";
	echo "\noptions: {title: {text: '$title'}}});\n</script>";
}
//,scales: {yAxes: [{ticks: {beginAtZero:false}}], xAxes: [{ticks: {beginAtZero:false}}]}
switch ($graphType)
{
	case 0:
		$toSelect = "nb";
		break;
	case 1:
		$toSelect = "nbPond";
		break;
	case 2:
		$toSelect = "nb10";
		break;
}
$yyFilter = ($yearIn == 0 ? "" : " and d.yyyy>='$yearIn'").($yearOut == 0 ? "" : " and d.yyyy<='$yearOut'");
$mmFilter = ($monthIn == 0 ? "" : " and d.mm>='".sprintf("%02d", $monthIn)."'").($monthOut == 0 ? "" : " and d.mm<='".sprintf("%02d", $monthOut)."'");
$dptFilter = $dpt == "00000" ? " and code is null" : " and code='".$dpt."'";
if (($ageRange == -1) && ($yearIn != 0) && ($yearIn == $yearOut))
// We will show every single age groups for a single year
{
	$ageFilter = " and d.age is not null";
	$group = "a.label";
	$firstOrder = " d.age,";
	$colTable = "agecol a";
	$colCol = "a.color";
	$colJoin = "a.age=d.age";
}
else
// We will show all years (for the desired ages or all)
{
	$ageFilter = $ageRange == -1 ? " and d.age is null" : " and d.age=".$ageRange;
	$group = "d.yyyy";
	$firstOrder = "";
	$colTable = "pop p";
	$colCol = "p.col2";
	$colJoin = "p.yyyy=d.yyyy";
}
//echo "select $group, d.yyyy, d.mm, d.dd, d.$toSelect, $colCol from $colTable, dstat".$dpt." d where $colJoin$dptFilter$yyFilter$mmFilter$ageFilter order by$firstOrder d.yyyy, d.mm, d.dd";
createChart($conn, "line", "myChart", "Year ".$yearIn." Deaths Per Day", 1300, 800, "select $group, d.yyyy, d.mm, d.dd, d.$toSelect, $colCol from $colTable, dstat".$dpt." d where $colJoin$dptFilter$yyFilter$mmFilter$ageFilter order by$firstOrder d.yyyy, d.mm, d.dd", "", array());
?>
<!--div width="200px" height="200px" style="width:200px;height=200px">
<canvas id="myChart" width="200" height="200"></canvas>
</div>
<script>
var chart = document.getElementById("myChart");
var ctx = chart.getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3]/*,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ]*/,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script-->
<?php
$conn->close();
echo "<br>Page generated in ".(time()-$startGen)." s";
?>
</body>
</html>
