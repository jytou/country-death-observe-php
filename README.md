This is the website part of the mortality observation project. You will need to get the Java project first in order to feed the database: https://gitlab.com/jytou/country-death-observe-java

Just install this project on a webserver with PHP and mysqli enabled.

Copy connect_host.ini.template to connect_myhost.ini (replace "myhost" by anything you like). Edit it to reflect the connection parameters to your database.

Then open the index.php file, and point the ini file to your database and reference the correct ini file in the index.php file:

```
$conn = connect("myhost", "mydatabaseschema");
```

where "myhost" is the same string than the one you chose when copying the connect.ini file.

Point your browser to the directory of the project in your website. Et voilà.